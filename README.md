# Library Management System

This is a simple Library Management System. Before running the system, please make sure to update the MongoDB database path.

## Setup Instructions

1. **Update MongoDB Database Path:**
   - Open `app.js` file.
   - Locate the MongoDB connection string (`mongoose.connect`).
   - Update the URL to match your MongoDB server path.

```javascript
mongoose.connect('mongodb://localhost:27017/library', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
