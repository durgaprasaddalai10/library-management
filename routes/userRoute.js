import express from "express";
import { handleSignup,handleSignIn } from "../controller/userController.js";
const router = express.Router();

router.post("/", handleSignup);
router.post("/login", handleSignIn);

export default router;
