import express from "express";
import { Book } from "../models/bookModel.js";
import qrCode from "qrcode";
import { restrictToLoggedInUserOnly } from "../middleware/authMiddleware.js";
import jwt from "jsonwebtoken";

const router = express.Router();
const generateQRCode = async (data) => {
  try {
    const qrCodeDataURL = await qrCode.toDataURL(data);
    return qrCodeDataURL;
  } catch (error) {
    throw error;
  }
};
// Route for Save a new Book
router.post("/", restrictToLoggedInUserOnly, async (request, response) => {
  try {
    const { title, author, publishYear } = request.body;

    // Validation
    if (!title || !author || !publishYear) {
      return response.status(400).send({
        message: "Send all required fields: title, author, publishYear",
      });
    }

    // Access user's information using the JWT token
    const token = request.headers.authorization;
    const decodedToken = jwt.verify(token, "your-secret-key");
    const createdBy = decodedToken.userId;

    const newBook = {
      title,
      author,
      publishYear,
      createdBy,
    };

    const book = await Book.create(newBook);

    // Generate QR code for the book information
    const qrCodeDataURL = await generateQRCode(JSON.stringify(book));

    // Update the book object to include the QR code data
    book.qrCode = qrCodeDataURL;

    // Save the modified book object
    await book.save();

    // Send the modified book object in the response, including the QR code
    return response
      .status(201)
      .send({ book: { ...book.toObject(), qrCode: qrCodeDataURL } });
  } catch (error) {
    console.log(error.message);
    response.status(500).send({ message: "Internal Server Error" });
  }
});

// For bulk request
router.post("/bulk", restrictToLoggedInUserOnly, async (request, response) => {
  try {
    const booksData = request.body;

    // Validation
    if (!Array.isArray(booksData)) {
      return response.status(400).send({
        message: "Invalid data format. Expecting an array of books.",
      });
    }

    // Validate each book in the array
    const invalidBooks = booksData.filter(
      ({ title, author, publishYear }) => !title || !author || !publishYear
    );

    if (invalidBooks.length > 0) {
      return response.status(400).send({
        message: "Each book must have title, author, and publishYear.",
        invalidBooks,
      });
    }

    // Access user's information using the JWT token
    const token = request.headers.authorization;
    const decodedToken = jwt.verify(token, "your-secret-key");
    const createdBy = decodedToken.userId;

    // Check for existing books in the database
    const existingBooks = await Book.find({
      $or: booksData.map(({ title }) => ({ title })),
    });

    if (existingBooks.length > 0) {
      return response.status(409).send({
        message: "Some of the books already exist in the database.",
        existingBooks,
      });
    }

    // Generate QR codes and create books in bulk
    const createdBooks = await Promise.all(
      booksData.map(async (bookData) => {
        const qrCodeDataURL = await generateQRCode(JSON.stringify(bookData));
        const bookWithQRCode = {
          ...bookData,
          qrCode: qrCodeDataURL,
          createdBy,
        };
        return bookWithQRCode;
      })
    );

    await Book.create(createdBooks);

    return response.status(201).json({
      message: "Books created successfully",
      createdBooks,
    });
  } catch (error) {
    console.log(error.message);
    response.status(500).send({ message: "Internal Server Error" });
  }
});
// Route for Get All Books from database
router.get("/", async (request, response) => {
  try {
    // Check if user is logged in
    if (!request.user.id) {
      return response
        .status(401)
        .json({ error: "Unauthorized - User not logged in" });
    }

    // If the user is logged in, proceed to fetch books posted by that user
    const userId = request.user.id;
    const books = await Book.find({ createdBy: userId });

    return response.status(200).json({
      count: books.length,
      data: books,
    });
  } catch (error) {
    console.log(error.message);
    response.status(500).send({ message: error.message });
  }
});

// Route for Get One Book from database by id
router.get("/:id", restrictToLoggedInUserOnly, async (request, response) => {
  try {
    const { id } = request.params;

    const book = await Book.findById(id);

    return response.status(200).json(book);
  } catch (error) {
    console.log(error.message);
    response.status(500).send({ message: error.message });
  }
});

// Route for Update a Book
router.put("/:id",restrictToLoggedInUserOnly, async (request, response) => {
  try {
    if (
      !request.body.title ||
      !request.body.author ||
      !request.body.publishYear
    ) {
      return response.status(400).send({
        message: "Send all required fields: title, author, publishYear",
      });
    }

    const { id } = request.params;

    const result = await Book.findByIdAndUpdate(id, request.body);

    if (!result) {
      return response.status(404).json({ message: "Book not found" });
    }

    return response.status(200).send({ message: "Book updated successfully" });
  } catch (error) {
    console.log(error.message);
    response.status(500).send({ message: error.message });
  }
});

// Route for Delete a book
router.delete("/:id", restrictToLoggedInUserOnly,async (request, response) => {
  try {
    const { id } = request.params;

    const result = await Book.findByIdAndDelete(id);

    if (!result) {
      return response.status(404).json({ message: "Book not found" });
    }

    return response.status(200).send({ message: "Book deleted successfully" });
  } catch (error) {
    console.log(error.message);
    response.status(500).send({ message: error.message });
  }
});

export default router;
