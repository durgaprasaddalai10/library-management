import jwt from "jsonwebtoken";

// Use a Map to store the user information with the user ID as the key
const userIdToUserMap = new Map();

export function setUser(userId, user) {
  // Use the userId as the key instead of the token
  userIdToUserMap.set(userId, user);
}

export function getUser(userId) {
  // Use the userId as the key instead of the token
  return userIdToUserMap.get(userId);
}
