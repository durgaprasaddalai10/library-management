import jwt from "jsonwebtoken";
import { getUser } from "../service/authService.js";

export async function restrictToLoggedInUserOnly(request, response, next) {
  const token = request.headers.authorization;

  console.log("Request Headers:", request.headers); // Add this line to log headers

  // Check if token is not available
  if (!token) {
    console.log("Token is not available");
    return response
      .status(401)
      .json({ error: "Unauthorized - Token not available" });
  }

  // If token is available, log it
  console.log("Token is available");

  try {
    // Verify the token
    const decodedToken = jwt.verify(token, "your-secret-key");

    // Get user using the userId from the decoded token
    const user = getUser(decodedToken.userId);

    // Check if user is not available
    if (!user) {
      console.log("User is not available");
      return response
        .status(401)
        .json({ error: "Unauthorized - User not found" });
    }

    // If user is available, log it
    console.log("User is available");
    console.log(user);

    // Attach the user object to the request for further use in the route
    request.user = user;

    // Continue to the next middleware or route handler
    next();
  } catch (err) {
    console.log("Invalid or expired token");
    // If the token is invalid or expired, handle the error
    if (err.name === "TokenExpiredError") {
      return response.status(401).json({ error: "Unauthorized - Token expired" });
    } else {
      return response.status(401).json({ error: "Unauthorized - Invalid token" });
    }
  }
}
