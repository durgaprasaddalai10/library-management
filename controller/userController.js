// import { v4 as uuidv4 } from "uuid";
import jwt  from "jsonwebtoken";
import { setUser } from "../service/authService.js";
import { User } from "../models/userModel.js";

export async function handleSignup(req, res) {
  const { name, email, password } = req.body;
  try {
    const createdUser = await User.create({
      name,
      email,
      password,
    });

    res.status(201).json({
      message: "User created successfully",
      user: {
        id: createdUser.id,
        name: createdUser.name,
        email: createdUser.email,
      },
    });
    console.log(createdUser);
  } catch (err) {
    console.log(err.message);
  }
}
export async function handleSignIn(req, res) {
  const { email, password } = req.body;

  try {
    // Find the user with the provided email in the database
    const existingUser = await User.findOne({ email, password });

    if (!existingUser) {
      // User with the provided email does not exist
      return res.status(401).json({ error: "Invalid credentials" });
    }

    // Generate a JWT token
    const token = jwt.sign({ userId: existingUser.id }, "your-secret-key", {
      expiresIn: "1h", // Set the expiration time as needed
    });

    // Use existingUser.id as the first argument for setUser
    setUser(existingUser.id, existingUser);

    // Send the JWT token in the response
    res.status(200).json({
      message: "Sign-in successful",
      user: {
        id: existingUser.id,
        name: existingUser.name,
        email: existingUser.email,
        token: token,
      },
    });
    console.log(existingUser)
  } catch (err) {
    console.error("Error during sign-in:", err);
    return res.status(500).json({ error: "Internal Server Error" });
  }}
